1. Objectifs
- Connaître des utilisations possible de Python. 
- Connaître des utilisations possible du module turtle
- Utiliser le module turtle

2. Pré-requis à cette activité
- Savoir saisir sur un clavier;
- Savoir ce qu'est un langage de programmation.


3. Durée de l'activité
Deux (02) heures.

4. Exercices cibles
Les exercices de niveau de difficulté * et **.

5. Description du déroulement de l'activité
- Le professeur manipule et invite à l'imiter;
- Il passe ensuite chez les élèves qui ont des difficultés pour les aider;

6. Anticipation des difficultés des élèves
- Préparer une description de la dmarche à suivre;
- Fournir la description de la démarche au élèves si nécessaire.

7. Gestion de l'hétérogénéïté
- Permettre à chacun d'avancer sur les exercices  selon ses capacités

